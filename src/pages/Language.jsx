import React, { Component } from "react";
import LanguageComponent from "../components/LanguageComponent";

class Language extends Component {
  state = {
    languageList: [
      {
        name: "HTML & CSS",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg",
      },
      {
        name: "JavaScript",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg",
      },
      {
        name: "React",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg",
      },
      {
        name: "Ruby",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg",
      },
      {
        name: "Ruby on Rails",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg",
      },
      {
        name: "Python",
        image:
          "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg",
      },
    ],
  };
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.languageList.map((language) => {
            return (
              <LanguageComponent name={language.name} images={language.image} />
            );
          })}
        </header>
      </div>
    );
  }
}

export default Language;
