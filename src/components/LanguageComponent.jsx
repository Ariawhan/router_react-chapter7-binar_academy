import React, { Component } from "react";

class LanguageComponent extends Component {
  state = { name: this.props.name, images: this.props.images };
  render() {
    return (
      <div className="language-item">
        <div className="language-name">{this.state.name}</div>
        <img
          className="language-image"
          src={this.state.images}
          alt={Math.floor(Math.random() * 200)}
        />
      </div>
    );
  }
}

export default LanguageComponent;
